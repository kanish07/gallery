const errorHandler = (error, req, res, next) => {
    if(error && error.error) {
        res.status(500).json({ error })
    }
    let errorObjToSend = {}
    if(typeof error === 'string') {
        errorObjToSend.error = error
    } else if(error.message) {
        errorObjToSend.error = error.message
    } else {
        errorObjToSend.error = error.toString()
    }    
    res.status(500).json(errorObjToSend)
}


const cors = async (req, res, next) => {
    try {
      
      res.set('Access-Control-Allow-Origin', '*')
      if (req.method === 'OPTIONS') {
        res.set('Access-Control-Allow-Methods', 'GET')
        res.set('Access-Control-Allow-Headers', 'Content-Type')
        res.set('Access-Control-Max-Age', '3600')
      }
      
      next(req, res) 
    }
    catch(e) {
      return res.status(500).json({ error:{code:500, message:'Something went wrong at cors'} })
    }
  }

module.exports = {
    errorHandler,
    cors
}