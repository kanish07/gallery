

module.exports = Model => {
    return {
        findAll: () => Model.find({}), 
        find: ({filter, projection, populatekey}) => Model.find(filter, projection).populate(populatekey),
        findById: id => Model.findById(id),
        create: record => Model.create(record),
        findOne: (filter, projection) => Model.findOne(filter, projection),
        aggregate: (obj) => Model.aggregate(obj),
    }
}
