const mongoose = require('mongoose')

const localdburi = 'mongodb://127.0.0.1:27017/image_db?retryWrites=true&w=majority'

mongoose.set('strictQuery', false)

module.exports = () => {
    return mongoose.connect(
        localdburi, 
        { useNewUrlParser: true, useUnifiedTopology: true }
    )
}