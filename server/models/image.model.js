const mongoose = require('mongoose')

module.exports = mongoose.model('Image', {
    type: { type: String, required: true },
    urls: { type: Array }
})