const express = require('express')
// const cors = require('cors')
const bodyParser = require('body-parser')

const connectDb = require('./config/db.js')
const imageRoutes = require('./controllers/image.controller')
const { errorHandler, cors } = require('./middlewares')

const app = express()
//middleware
// app.use(cors)
app.use(bodyParser.json())
app.use(express.static(__dirname))

app.use('/api/', imageRoutes)
app.use(errorHandler)

async function startup() {
    await connectDb()
    console.log('db connection succeeded')
    app.listen(3000,() => console.log('server started at 3000'))
}

startup()

