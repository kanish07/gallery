const express = require('express')
const router = express.Router()

const ImageModel = require('../models/image.model')
const crudMethods = require('../utils/crudMethods')
const Image = crudMethods(ImageModel)

router.get('/', async function(req,res, next){
    try {
        if(!req?.query?.type) {
            throw "query not found"
        }
        const data = await Image.findOne({type: req.query.type})
        return res.status(200).json({status: 200, message: 'success', data})
    } catch (error) {
        return next(error)
    }
})

module.exports = router