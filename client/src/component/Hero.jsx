import { useEffect, useState } from 'react'
import axios from 'axios'
import './Hero.css'

function Hero() {
    const [images, setImages] = useState([])

    const fetchData = async () => {
        try {
            const resp = await axios.get("/api?type=hero")
            setImages(resp.data.data.urls)
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="container">
            <h1 style={{ textAlign: "center" }}>Locations</h1>
            <div className="gallery-wrap">
                {
                    images.map((img, i) => {
                        return (
                            <div key={i} className={`itemx item-${i + 1}`} style={{
                                backgroundImage: `url(${img})`
                            }}>
                                {/* <img src={img} /> */}
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}

export default Hero