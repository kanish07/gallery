import { useEffect, useState } from 'react';
import axios from 'axios'
import './Scroll.css'


function Scroll() {
    const [images, setImages] = useState([])
    const [isOpen, setIsOpen] = useState(false)
    const [openImageId, setOpenImageId] = useState(100)

    const fetchData = async () => {
        try {
            const resp = await axios.get("/api?type=carousel")
            setImages(resp.data.data.urls)
        } catch (error) {
            console.error(error);
        }
    }

    const onClickHandler = (i) => {
        debugger
        setIsOpen(true)
        setOpenImageId(i)
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className='scroll-animations-example'>
            <h1 style={{ textAlign: "center" }}>
                <span style={{ linearGradient: "90deg, #7928CA, #FF0080" }} >Villas</span>
            </h1>
            <div className='scrollsection'>
                {
                    images.map((img, i) => {
                        return (
                            <div className='item' key={i} onClick={() => onClickHandler(i)}>
                                <img className='image' src={img} />
                            </div>
                        )
                    })
                }
            </div>

            {
                isOpen
                    ? dialog(images, openImageId, setIsOpen)
                    : null
            }
        </div>


    )
}



function dialog(images, openImageId, setIsOpen) {
    return (
        <div id="modal" class="modal" onClick={() => { setIsOpen(false) }}>
            <span id="modal-close" class="modal-close" onClick={() => { setIsOpen(false) }}>&times;</span>
            <img id="modal-content" class="modal-content" src={images[openImageId]} />
            <div id="modal-caption" class="modal-caption">Some Caption</div>
        </div>
    )
}


export default Scroll