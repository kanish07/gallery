import Scroll from './component/Scroll';
import Hero from './component/Hero';



function App() {
  return (
    <>
      <Hero />
      <Scroll />

    </>
  )
}

export default App;
